#!/bin/bash
#
#   make-devel-src-rpm.sh
#
CMD="$1"

echo "Info: Creating source tarball"

. ${BUILDER_TOP_DIR}/Builder/version.info
V=${MAJOR}.${MINOR}.${PATCH}
rm -rf /tmp/pysvn-workbench-${V}

BUILD=$( svnversion .. )

KIT_BASENAME=pysvn-workbench-${V}

rm -rf tmp
mkdir -p tmp
pushd tmp
svn export --quiet ${BUILDER_TOP_DIR} ${KIT_BASENAME}

cat <<EOF >pysvn-workbench-${V}/Builder/version.info
MAJOR=${MAJOR}
MINOR=${MINOR}
PATCH=${PATCH}
BUILD=${BUILD}
EOF

tar czf ${KIT_BASENAME}.tar.gz ${KIT_BASENAME}
popd

echo "Info: Creating SRPM for ${KIT_BASENAME}"

sudo \
    mock \
        --buildsrpm --dnf \
        --spec pysvn-workbench.spec \
        --sources tmp/${KIT_BASENAME}.tar.gz

MOCK_ROOT=$( sudo mock -p )
MOCK_BUILD_DIR=${MOCK_ROOT}/builddir/build
ls -l ${MOCK_BUILD_DIR}/SRPMS

set $(tr : ' ' </etc/system-release-cpe)
case $4 in
fedora)
    DISTRO=fc$5
    ;;
*)
    echo "Error: need support for distro $4"
    exit 1
    ;;
esac

SRPM_BASENAME="${KIT_BASENAME}-1.${DISTRO}"

cp -v "${MOCK_BUILD_DIR}/SRPMS/${SRPM_BASENAME}.src.rpm" tmp

echo "Info: Creating noarch RPM"
sudo \
    mock \
        --rebuild --dnf \
            "tmp//${SRPM_BASENAME}.src.rpm"

ls -l ${MOCK_BUILD_DIR}/RPMS

cp -v "${MOCK_BUILD_DIR}/RPMS/${SRPM_BASENAME}.noarch.rpm" tmp

echo "Info: Results in ${PWD}/tmp:"
ls -l tmp

if [ "$CMD" = "--install" ]
then
    echo "Info: Installing RPM"
    sudo dnf -y remove pysvn-workbench
    sudo dnf -y install "tmp/${SRPM_BASENAME}.noarch.rpm"
fi
