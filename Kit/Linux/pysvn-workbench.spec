Name:           pysvn-workbench
Version:        1.7.1
Release:        1%{?dist}
Summary:        PySVN WorkBench IDE

License:        ASL 1.1
URL:            http://pysvn.tigris.org/
Source0:        http://pysvn.barrys-emacs.org/source_kits/%{name}-%{version}.tar.gz

BuildArch:      noarch
BuildRequires:  python
Requires:       wxPython >= 2.8
Requires:       pysvn >= 1.8.0
Requires:       gettext

%description

PySVN WorkBench Features
 * Easy to learn and use
 * Most subversion client operations in a GUI
 * Enhanced subversion operations (rename of modified files etc)
 * Support software development workflow
 * Builtin GUI diff showing line and character diffs
 * Ability to diff between revisions of a file's history
 * Runs on Windows, Mac OS X and Unix platforms
 * Implemented in Python, allowing customization 

%prep
%setup -q -n %{name}-%{version}

%build
%configure
make all PYTHON=%{__python}

%install
rm -rf $RPM_BUILD_ROOT
%make_install


%files
%defattr(-, root, root, -)
%doc Docs/WorkBench.html Docs/WorkBench_files/*.png
%license LICENSE.txt
/usr/bin/pysvn-workbench
/usr/share/pysvn-workbench
/usr/share/applications/pysvn-workbench.desktop
%attr(0644,root,root) %{_mandir}/man1/pysvn-workbench.1.gz

%changelog
* Thu Mar 17 2016 barry scott <barry@barrys-emacs.org> 1.7.1-1
- Take upstream fixes

* Sun Aug 23 2015 barry scott <barry@barrys-emacs.org> 1.7.0-1
- First version
