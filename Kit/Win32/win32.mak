all: kit

kit: tmp\workbench.iss info_before.txt copy_setup.cmd
	copy ..\..\LICENSE.txt tmp\workbench_LICENSE.txt
	copy ..\..\Source\bin\wb.exe tmp\WorkBench.exe
	copy ..\..\Source\bin\wb.exe.manifest tmp\WorkBench.exe.manifest
	copy k:\subversion\vcredist_x86_2008.exe tmp
	"c:\Program Files (x86)\Inno Setup 5\ISCC.exe" tmp\workbench.iss
	tmp\setup_copy.cmd

info_before.txt: tmp\workbench.iss

copy_setup.cmd: tmp\workbench.iss

tmp\workbench.iss: setup_kit_files.py workbench.iss
	if exist tmp rmdir /s /q tmp
	mkdir tmp
	$(PYTHON) setup_kit_files.py

debug:
	"c:\Program Files (x86)\Inno Setup 5\Compil32.exe" tmp\workbench.iss

clean:
	if exist tmp rmdir /s /q tmp
