'''
 ====================================================================
 Copyright (c) 2003-2011 Barry A Scott.  All rights reserved.

 This software is licensed as described in the file LICENSE.txt,
 which you should have received as part of this distribution.

 ====================================================================

    wb_ids.py

'''
import wx

id_View_Refresh = wx.NewId()
id_View_AutoRefresh = wx.NewId()
id_View_ControlledFiles = wx.NewId()
id_View_IgnoredFiles = wx.NewId()
id_View_UncontrolledFiles = wx.NewId()
id_View_Recursive = wx.NewId()
id_View_OnlyChanges = wx.NewId()
id_ClearLog = wx.NewId()
id_SelectAll = wx.NewId()

id_View_Diff_WbDiff = wx.NewId()
id_View_Diff_ExtGuiDiff = wx.NewId()
id_View_Diff_ExtTextDiff = wx.NewId()
id_View_Diff_SvnDiff = wx.NewId()

id_Project_Add = wx.NewId()
id_Project_Update = wx.NewId()
id_Project_Delete = wx.NewId()

id_Bookmark_Add = wx.NewId()
id_Bookmark_Manage = wx.NewId()

id_Command_Shell = wx.NewId()
id_File_Browser = wx.NewId()

id_Shell_Open = wx.NewId()

id_File_Edit = wx.NewId()
id_SP_NewFile = wx.NewId()

id_SP_ConflictMenu = wx.NewId()
id_SP_DetailsMenu = wx.NewId()

id_SP_EditCopy = wx.NewId()
id_SP_EditCut = wx.NewId()
id_SP_EditPaste = wx.NewId()

id_SP_Add = wx.NewId()
id_SP_Annotate = wx.NewId()
id_SP_Checkin = wx.NewId()
id_SP_Checkout = wx.NewId()
id_SP_CheckoutTo = wx.NewId()
id_SP_Cleanup = wx.NewId()
id_SP_Delete = wx.NewId()
id_SP_DiffMineNew = wx.NewId()
id_SP_DiffOldMine = wx.NewId()
id_SP_DiffOldNew = wx.NewId()
id_SP_DiffRevisionRevision = wx.NewId()
id_SP_DiffWorkBase = wx.NewId()
id_SP_DiffWorkHead = wx.NewId()
id_SP_DiffWorkBranchOriginBase = wx.NewId()
id_SP_DiffWorkBranchOriginHead = wx.NewId()
id_SP_History = wx.NewId()
id_SP_Info = wx.NewId()
id_SP_Lock = wx.NewId()
id_SP_Mkdir = wx.NewId()
id_SP_Properties = wx.NewId()
id_SP_Rename = wx.NewId()
id_SP_Resolved = wx.NewId()
id_SP_Revert = wx.NewId()
id_SP_Unlock = wx.NewId()
id_SP_Update = wx.NewId()
id_SP_UpdateTo = wx.NewId()
id_SP_Upgrade = wx.NewId()
id_SP_CreateTag = wx.NewId()
id_SP_CreateBranch = wx.NewId()

id_SP_Report_Updates = wx.NewId()
id_SP_Report_LocksWc = wx.NewId()
id_SP_Report_LocksRepos = wx.NewId()
id_SP_Report_BranchChanges = wx.NewId()

