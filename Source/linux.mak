#
#	makefile WorkBench
#
all: locale/en/LC_MESSAGES/pysvn_workbench.mo wb_version.py wb_images.py

locale/en/LC_MESSAGES/pysvn_workbench.mo: wb_version.py wb_images.py
	./make-pot-file.sh
	./make-po-file.sh en
	./make-mo-files.sh locale

include wb_common.mak
