'''

 ====================================================================
 Copyright (c) 2003-2006 Barry A Scott.  All rights reserved.

 This software is licensed as described in the file LICENSE.txt,
 which you should have received as part of this distribution.

 ====================================================================

	wb_version.py.template

'''
major = 1
minor = 8
patch = 2
build = 1931
