'''
 ====================================================================
 Copyright (c) 2003-2010 Barry A Scott.  All rights reserved.

 This software is licensed as described in the file LICENSE.txt,
 which you should have received as part of this distribution.

 ====================================================================


    wb_diff_main.py

'''

VERSION_STRING = "Uncontrolled"

import sys

# make sure that we get 2.8 or 3.0 and not an earlier version
if not hasattr(sys, 'frozen'):
    import wxversion
    wxversion.select( ['3.0', '2.8'] )

def noTranslate(msg):
    return msg

import __builtin__
__builtin__.__dict__['T_'] = noTranslate
__builtin__.__dict__['U_'] = noTranslate

import wx
import wb_diff_frame
import wb_preferences
import wb_platform_specific



class WbDiffApp(wx.App):
    def __init__( self, file1, file2 ):
        self.file1 = file1
        self.file2 = file2
        self.log = self
        self.prefs = wb_preferences.Preferences(
                self,
                wb_platform_specific.getPreferencesFilename(),
                wb_platform_specific.getOldPreferencesFilename() )

        wx.App.__init__( self, 0 )

    def OnInit( self ):
        self.frame = wb_diff_frame.DiffFrame( self, None, self.file1, self.file1, self.file2, self.file2 )
        self.frame.Show( True )
        self.SetTopWindow( self.frame )

        return True

    def info( self, *arg ):
        pass

        
def main():
    if len(sys.argv) < 3:
        print 'Usages: wb_diff file1 file2'
        return 1

    file1 = sys.argv[1]
    file2 = sys.argv[2]

    diff_app = WbDiffApp( file1, file2 )
    diff_app.MainLoop()
    return 0

if __name__ == '__main__':
    main()
