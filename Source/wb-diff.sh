#!/bin/bash
export PYSVN_WORKBENCH_STDOUT_LOG=$(tty)

SOURCEDIR=$( dirname $0 )

if [ "$PYTHONPATH" = "" ]
then
	export PYTHONPATH=${SOURCEDIR}
else
	export PYTHONPATH=${SOURCEDIR}:$PYTHONPATH
fi

PYTHON=${PYTHON:-python}
BASENAME=$( basename ${PYTHON} )
SUFFIX=${X#python*}
DIRNAME=$( dirname ${PYTHON} )

if [ "${DIRNAME}" != "" ]
then
    DIRNAME=${DIRNAME}/
fi
PYTHONW=${DIRNAME}pythonw${SUFFIX}

if [ -e ${PYTHONW} ]
then
    ${PYTHONW} ${SOURCEDIR}/wb_diff_main.py $*
else
    ${PYTHON} ${SOURCEDIR}/wb_diff_main.py $*
fi
