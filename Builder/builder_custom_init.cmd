rem
rem builder_custom_init.cmd
rem
if "%BUILDER_TOP_DIR%" == "" (
    echo Error: BUILDER_TOP_DIR is not set
    exit /b 1
)

if not "%1" == "" set PYTHON_VER=%1
if "%PYTHON_VER%" == "" set PYTHON_VER=2.7

if not "%2" == "" set SVN_VER_MAJ_MIN=%2
if "%SVN_VER_MAJ_MIN%" == "" set /p SVN_VER_MAJ_MIN="Build against Subversion Version [maj.min]: "
if "%SVN_VER_MAJ_MIN%" == "" goto :eof

rem python2 does not work with cp65001
chcp 1252

rem Save CWD
pushd .

pushd ..\..\ReleaseEngineering\Windows
call ..\..\ReleaseEngineering\Windows\build-config.cmd %PYTHON_VER% %SVN_VER_MAJ_MIN% win32
popd

echo Info: In release engineering build mode TARGET %TARGET%
set PYSVN_PYTHONPATH=%TARGET%\py-%PYTHON_VER%-pysvn\Source
set PYTHONPATH=%TARGET%\py-%PYTHON_VER%-pysvn\Source;%BUILDER_TOP_DIR%\Source
echo PYTHONPATH %PYTHONPATH%

set MEINC_INSTALLER_DIR=%BUILDER_TOP_DIR%\Import\MEINC_Installer-%MEINC_INSTALLER_VER%-py%PYTHON_VER:.=%-win32
set INCLUDE=%MEINC_INSTALLER_DIR%;%INCLUDE%

rem PATH
rem must have %PYTHON% on the path as meinc installer uses the path to find python
rem
rem gettext etc from http://mlocati.github.io/gettext-iconv-windows/
rem installed into "c:\Program Files\gettext-iconv"
rem
rem the DLLs for the SVN we are building againstmust be on the path to allow
rem the svn_version to be determined
for /d %%X in (%PYTHON%) do PATH %%~dpX;c:\Program Files\gettext-iconv;%SVN_BIN%;%PATH%

%PYTHON% -c "import sys;print 'Info: Python Version',sys.version"
%PYTHON% -c "import pysvn;print 'Info: pysvn Version',pysvn.version,'svn version',pysvn.svn_version"
popd
