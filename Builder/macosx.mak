build:
	cd ../Source && make -f macosx.mak all
	cd ../Kit/MacOSX && ./build.sh

clean:
	cd ../Source && make -f macosx.mak clean
	rm -rf  ../Kit/MacOSX/tmp
